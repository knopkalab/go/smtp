package smtp

import (
	"errors"
	"net/smtp"
	"strconv"
	"strings"

	"gitlab.com/knopkalab/go/konfig"
)

// ClientConfig for smtp sender
type ClientConfig struct {
	Server string `desc:"smtp server, auto lookup by default"`
	Port   uint16 `desc:"possible: 25/2525 (unsafe), 587 (TLS), 465 (SSL)"` // 587 by default

	Username konfig.AES `desc:"required, usually email"`
	Password konfig.AES `desc:"optional, raw password"`

	FromMail string `desc:"optional, Username by default"`
	FromName string `desc:"optional, Username prefix by default"`

	DefaultTo  []string `desc:"recipients"`
	DefaultCc  []string `desc:"recipients, carbon copy"`
	DefaultBcc []string `desc:"recipients, blind carbon copy"`

	SendQueueSize int

	from string    // "FromName <From>""
	auth smtp.Auth // plain
}

// ValidAndRepair smtp config
func (c *ClientConfig) ValidAndRepair() error {
	spaceReplacer := strings.NewReplacer(" ", "", "\t", "")

	c.Username.Val = spaceReplacer.Replace(c.Username.Val)
	if c.Username.Val == "" {
		return errors.New("smtp.client config.username no set")
	}
	c.FromMail = spaceReplacer.Replace(c.FromMail)
	if c.FromMail == "" {
		c.FromMail = c.Username.Val
	}
	c.FromName = strings.TrimSpace(c.FromName)
	if c.FromName == "" {
		if i := strings.LastIndex(c.FromMail, "@"); i != -1 {
			c.FromName = c.FromMail[:i]
		}
	}
	c.from = c.FromName + " <" + c.FromMail + ">"

	if c.SendQueueSize < 1 {
		c.SendQueueSize = 1000
	}
	if c.Port == 0 {
		c.Port = 587
	}
	if c.Server == "" {
		if i := strings.LastIndex(c.Username.Val, "@"); i != -1 {
			c.Server = Lookup(c.Username.Val[i+1:])
		} else {
			return errors.New("smtp.client config.username invalid")
		}
	}

	host := c.Server
	if i := strings.IndexRune(c.Server, ':'); i != -1 {
		host = c.Server[:i]
	} else {
		c.Server += ":" + strconv.Itoa(int(c.Port))
	}
	c.auth = smtp.PlainAuth("", c.Username.Val, c.Password.Val, host)

	return nil
}
